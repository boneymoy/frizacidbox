from __future__ import print_function
from PIL import Image

import logging
import os
import subprocess
import sys

import gphoto2 as gp

def capture():
    logging.basicConfig(
        format='%(levelname)s: %(name)s: %(message)s', level=logging.WARNING)
    # callback_obj = gp.check_result(gp.use_python_logging())
    camera = gp.Camera()
    camera.init()
    print("#####################################################################################################################################################")
    print('                                                                  Processing image')
    print("#####################################################################################################################################################")
    file_path = camera.capture(gp.GP_CAPTURE_IMAGE)
    print('Camera file path: {0}/{1}'.format(file_path.folder, file_path.name))
    #target = os.path.join('/home/tk/Documents/different_approach/image', file_path.name)
    target = os.path.join('/tmp', file_path.name)
    print('Copying image to', target)
    camera_file = camera.file_get(
        file_path.folder, file_path.name, gp.GP_FILE_TYPE_NORMAL)
    camera_file.save(target)
    #subprocess.call(['xdg-open', target])
    camera.exit()
    return target[:-3] + 'jpg'

def downsize_img(file_path, downscale=4):
    img = Image.open(file_path)
    img = img.resize((int(img.size[0] / downscale), int(img.size[1] / downscale)),Image.ANTIALIAS)
    img.save(file_path)


if __name__ == "__main__":
    file_path = capture()
    downsize_img(file_path)
