import os
import sys
import uuid
import time
import shutil
import timeit
import tkinter
import threading
import screeninfo
import subprocess
import numpy as np
import pickle as pkl
from PIL import Image, ImageTk
from termios import tcflush, TCIFLUSH


import camera
import deepdream
import fullscreen

webcam = False
# try to load image counter
# if it fails initialize img_counter with 0
pkl_file = "img_count.pkl"
try:
    img_count = pkl.load(pkl_file)
except Exception:
    img_count = 0
# set path of uid list
UID_PATH = 'data/uid_list.txt'
# set path of dream images
DREAM_PATH = 'image/'
# wait photo_timer seconds after keypress
# before image is taken
photo_timer = 5 #s


def print_current_timer(photo_timer, counter):
    """
    print some hashtags for user to see
    (loop just for less typing)
    """
    for j in range(15):
        # print in same line
        print("##########", end="", flush=True)
    # end printing in same line
    print("")
    for j in range(7):
        print("#########", end="", flush=True)
    print(f"     Wait {photo_timer - i} seconds     ", end="", flush=True)
    for j in range(7):
        print("#########", end="", flush=True)
    print("")
    for j in range(15):
        print("##########", end="", flush=True)
    print("")

if __name__ == "__main__":
    """
    starts main loop
        waits for user keypress
        image is taken through camera -> see camera.py
        image is sent to deepdream algorithm -> see deepdream.py
        alternated image is shown in fullscreen
        !!!original image is overwritten in next loop!!!
        shown image can be closed with another keypress
    """
    while True:
        # kill running gphoto processes for stability
        os.system('pkill gphoto')
        for i in range(3):
            for j in range(5):
                print("##########", end="")
        tcflush(sys.stdin, TCIFLUSH)
        input("                                                           Press the button to take a photo...")
        for i in range(3):
            for j in range(5):
                print("##########", end="")
        tcflush(sys.stdin, TCIFLUSH)
        if True:
            # start timer for whole process
            start = timeit.default_timer()
            # print timer every second
            for i in range(photo_timer):
                print_current_timer(photo_timer, i)
                time.sleep(1)
            ########################################
            # capture image through camera
            ########################################
            try:
                file_path = camera.capture()
            except Exception:
                print("ERROR TAKING PICTURE")
                print("!!!!!!TRY AGAIN!!!!!")
                print("IF IT STILL DOES NOT WORK PLZZ CONTACT SOMEONE WITH BATIK SHIRT")
                continue

            # for debugging
            # print(file_path)
            # file_path = 'image/Pluto.jpg'
            # downsize image to minimize computational load
            camera.downsize_img(file_path, downscale=4)
            # create uid as password to later download image through telegramBot
            uid = uuid.uuid4().hex[:8].upper()
            # write uid to file for telegramBot to check
            # TODO check if file exists
            try:
                with open('data/uid_list.txt','a') as file:
                    file.write('\n' + uid)
            except Exception:
                print("UID file does not exist")
                print("Create new uid list")
            # send image to deepdream algorithm
            deepdream.run_deepdream(file_path=file_path)
            # create filename with uid
            file_path_uid = f'image/dream/{uid}.jpg'
            # move alternated image to image folder with uid_name
            shutil.move(file_path, file_path_uid)
            # stop timer to check computation time
            stop = timeit.default_timer()
            print(f'Process took: {round(stop - start, 2)}s')
            # open alternated image as PIL Image
            img = Image.open(file_path_uid)
            # call function to show image in fullscreen
            fullscreen.showPIL(img, uid)

        # load image counter and iterate
        try:
            with open(pkl_file, 'rb') as file:
                img_count = pkl.load(file)
        except Exception:
            os.system(f'touch {pkl_file}')
            img_count = 0
        img_count += 1
        with open(pkl_file, 'wb') as file:
            pkl.dump(img_count, file)
        print("Image No.: ", img_count)
        # start loop from beginning
